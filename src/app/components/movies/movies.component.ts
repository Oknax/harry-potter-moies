import {Component, OnInit} from '@angular/core';
import {AsyncPipe, NgForOf} from "@angular/common";
import {MovieService} from "../../shared/services/movie.service";
import {Movie} from "../../models/movie";
import {BudgetFormatPipe} from "../../shared/pipes/budget-format.pipe";
import {DurationFormatPipe} from "../../shared/pipes/duration-format.pipe";
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {combineLatest, map, Observable, startWith} from "rxjs";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-movies',
  standalone: true,
  imports: [
    NgForOf, BudgetFormatPipe, DurationFormatPipe, ReactiveFormsModule, AsyncPipe, RouterLink
  ],
  templateUrl: './movies.component.html',
  styleUrl: './movies.component.css'
})
export class MoviesComponent{

  searchForm = this.fb.nonNullable.group({
    title: [''],
    release: ['']
  })

  movies$: Observable<Movie[]> = this.getMovies();

  constructor(private movieService:MovieService, private fb:  FormBuilder) {
  }

 private getMovies(): Observable<Movie[]>{
   const movies$ = this.movieService.getMovies();
   const search$ = combineLatest([
      this.searchForm.controls.title.valueChanges.pipe(startWith('')),
      this.searchForm.controls.release.valueChanges.pipe(startWith(''))
 ]);

   return combineLatest([movies$, search$])
     .pipe(
       map(([movies, [title, release]]) => movies.filter(movie => {
           const titleMatch = movie.title.toLowerCase().includes(title.toLowerCase())
           const releaseMatch =  movie.release_date.toLowerCase().includes(release.toLowerCase())
           return titleMatch && releaseMatch;
         })
       )
   );

 }

}
