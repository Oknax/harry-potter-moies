import {Component, Input, OnInit} from '@angular/core';
import {MovieDetail} from "../../../models/movieDetail";
import {MovieService} from "../../../shared/services/movie.service";
import {Observable, switchMap} from "rxjs";
import {AsyncPipe, CommonModule} from "@angular/common";
import {ActivatedRoute, ParamMap, Router, RouterLink} from "@angular/router";
import {BudgetFormatPipe} from "../../../shared/pipes/budget-format.pipe";
import {DurationFormatPipe} from "../../../shared/pipes/duration-format.pipe";

@Component({
  selector: 'app-movie-detail',
  standalone: true,
  imports: [
    AsyncPipe, CommonModule, BudgetFormatPipe, DurationFormatPipe, RouterLink
  ],
  templateUrl: './movie-detail.component.html',
  styleUrl: './movie-detail.component.css'
})
export class MovieDetailComponent implements OnInit{
  movie$: Observable<MovieDetail>;

  constructor(private movieService: MovieService,
              private route: ActivatedRoute,
              private router: Router,) {
  }

  ngOnInit(): void {
    this.movie$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.movieService.getMovieById(params.get('id')!))
    );
  }

}
