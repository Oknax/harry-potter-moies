import { Injectable } from '@angular/core';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {Observable} from "rxjs";
import {Movie} from "../../models/movie";
import {MovieDetail} from "../../models/movieDetail";

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http:HttpClient) { }

  public getMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>('/movies');
  }

  public getMovieById(id: string): Observable<MovieDetail> {
    return this.http.get<MovieDetail>('/movies/' + id)
  }
}
