import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'budgetFormat',
  standalone: true
})
export class BudgetFormatPipe implements PipeTransform {
  transform(value: string): string {
    return `$${value} millions`;
  }

}
