import { Routes } from '@angular/router';
import {MoviesComponent} from "./components/movies/movies.component";
import {MovieDetailComponent} from "./components/movies/movie-detail/movie-detail.component";

export const routes: Routes = [
  {
    path: '',
    component: MoviesComponent,
  },
  {
    path:'movie/:id',
    component: MovieDetailComponent
  },
  {
    path: '**',
    redirectTo: '',
  }
];
