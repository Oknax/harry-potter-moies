import {Movie} from "./movie";

export interface MovieDetail {
  id: string,
  title: string,
  duration: string,
  budget: string,
  release_date: string,
  box_office: string,
  cinematographers: string[],
  poster: "https://www.wizardingworld.com/images/products/films/rectangle-10.png",
  producers: string[],
  summary: string
}


